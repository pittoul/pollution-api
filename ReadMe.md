# POUR DEMARRER AVEC DOCKER :

Vérifier l'état des dockers avec la commande : `docker ps`
qui listera les dockers actifs. En principe, ils démarrent avec le serveur.
Si ce n'est pas le cas : 
<br>
<br>


1 - Modifier les variables d'environnement pour la connexion à la BDD :

        Commenter/Décommenter la ligne correspondante dans le fichier `.env`

2 - Reconstruire/mettre à jour l'image :

                docker build . -t burns/node-api

3- Déployer l'image de l'app avec sa base de données :

                docker-compose up -d

...Le projet est visible sur le port 7999 à l'adresse :  
`http://[IP-LOCALE]:7999/datas`

Si jamais des erreurs sont commises dans les dockers : 

                docker-compose down --remove-orphans
                docker-compose rm

<br>
<br>

# POUR DEMARRER SANS DOCKER :

## 1- Stopper les dockers qui seraient entrain de tourner :
               
- pour voir quels dockers tournent :
               
                docker ps 

 - pour les stopper, aller dans le dossier racine du projet et :

                docker-compose stop

Un nouveau `docker ps` permet de vérifier que les dockers sont bien arrêtés.


<br>
<br>

## 2- Modifier les variables d'environnement pour la connexion à la BDD :

- Commenter/Décommenter la ligne correspondante dans le fichier `.env` pour établir la connexion àla BDD.


<br>
<br>

## 3- Démarrer le projet :

Afin de pouvoir démarrer les services nécessaire, nous utilisaons des `screen` (https://doc.ubuntu-fr.org/screen). Ceux-ci permettent de travailler en mode "détaché", ainsi, lorsqu'une fenêtre de terminal est fermée, les services démarrés ne sont pas stoppés.


- Dans le dossier racine , ouvrir 2 screens :

        screen -S mong
        mongod
        [ctrl] + [a]  puis c
        screen -d mong
        screen -S nod
        cd /home/bruno/projects/api-capteurs/
        node .
        [ctrl] + [a]  puis c
        screen -d nod

Et voilà !
Au prochain démarrage d serveur, il faudra redémarrer l'api avec cette même procédure.
:)

On peut changer le port exposé dans le fichier `.env`. 
Attention à mettre le Firewall à jour ! Par défaut, j'ai mis le 7999.

...Le projet est visible sur le port 7999 à l'adresse :  
`http://[IP-LOCALE]:7999/datas`

<br>
<br>
<br>
<br>

# REMARQUE : 
<span style="color:red">
Les BDD DOCKER et sans docker sont 2 bdd différentes ! Leur contenu est donc différent selon le mode de démarrage du projet !
</span>
<br>
<br>
<br>


# Ce projet est un serveur web Node/Express exposant une API RestFul

pour le démarrer après un `git pull`: 

                npm install
                node .
                (et dans un autre terminal) mongod 


## Ressources :

- Techno Front : les "handlebars" https://www.npmjs.com/package/express-handlebars




## TODO
- Mettre un swagger : https://www.npmjs.com/package/swagger-ui-express



## Mongo DB :

Installer mongo en dur :

        sudo apt install mongodb-clients
        sudo apt instll mongodb-server-core

- créer le dossier /data/db à la racine du système : / , avec `sudo mkdir /data/db`
- donner les droits rw : `sudo chown -R $USER /data/db`

- En local, démarrer mongo : `mongod`. Pour aller en console : `mongo`


# LINKS
- https://gitlab.com/pittoul/pollution-api
- https://www.it-swarm-fr.com/fr/node.js/comment-gerer-les-connexions-mongodb-dans-une-application-web-node.js/1066880814/

-       https://koistya.medium.com/how-to-open-database-connections-in-a-node-js-express-app-e14b2de5d1f8