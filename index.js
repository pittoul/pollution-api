/**
 *
 * Ce fichier est le SERVEUR express
 *
 */
const multer = require("multer");
const csv = require("fast-csv");
const fs = require("fs");
const dotenv = require("dotenv");
dotenv.config();
const engine = require("express-handlebars");
const express = require("express");
const bodyParser = require("body-parser");
const MongoClient = require("mongodb").MongoClient;
const ObjectID = require("mongodb").ObjectId;
const { json } = require("express");
const app = express();

app.engine("handlebars", engine.engine());
app.set("view engine", "handlebars"); // Moteur de template. Il y a aussi à disposition un moteur html...
app.set("views", "./views");
app.set("port", process.env.APP_PORT);
app.use(express.static(__dirname + "/public"));

// Set global directory
global.__basedir = __dirname;

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

const url = process.env.DATABASE_URL;
const dbName = process.env.DBNAME;

// Multer Upload Storage
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, __basedir + "/uploads/");
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname);
  },
});

// Filter for CSV file
const csvFilter = (req, file, cb) => {
  if (file.mimetype.includes("csv")) {
    cb(null, true);
  } else {
    cb("Please upload only csv file.", false);
  }
};
const upload = multer({ storage: storage, fileFilter: csvFilter });

let db;
MongoClient.connect(url, function (err, client) {
  db = client.db(dbName);
  console.log("Connexion à la base de données effectuée");
});

// Upload CSV file using Express Rest APIs
app.post("/upload", upload.single("file"), (req, res) => {
  try {
    if (req.file == undefined) {
      logTruc("Please upload a CSV file!\n");
      return res.status(400).send({
        message: "Please upload a CSV file!",
      });
    }

    // Import CSV File to MongoDB database
    let csvData = [];
    let filePath = __basedir + "/uploads/" + req.file.filename;
    console.log(filePath);
    logTruc(filePath);
    let readStream = fs.createReadStream(filePath);
    readStream
      .pipe(csv.parse({ headers: true }))
      .on("data", (row) => {
        csvData.push(row);
        logTruc('un row est poussé....');
      })
      .on("end", () => {
        console.log("data extraites du csv !");
        logTruc("data extraites du csv !");
        console.log(csvData);
        logTruc(JSON.stringify(csvData));
        db.collection("pollution").insertMany(csvData, function (err, result) {
          if (err) {
            res.status(500).json(err);
            logTruc("erreur insertion db");
          } else{
            res.status(200).send({
              message: "On en est là " + req.file.originalname,
            });
            logTruc("Insertion ok");
          }
        });
      })
      .on("close", function (err) {
        readStream.destroy((err) => {
          if (err) {
            console.log("error in destroying the stream " + err);
            logTruc("error in destroying the stream " + err.message);
          }
        });
        fs.unlink(filePath, function (err) {
          if (err) {
            console.log(`Error in deleting the file: ${err}.`);
            logTruc(`Error in deleting the file: ${err}.`);
          } else {
            console.log(`Successfully deleted the file.`);
            logTruc(`Successfully deleted the file.`);
          }
        });
      });
  } catch (error) {
    console.log("catch error-", error);
    logTruc("catch error-", error.message);
    res.status(500).send({
      message:
        "Upload impossible du fichier: " +
        req.file.originalname +
        "\nErreur : " +
        error.message,
    });
  }
});

// Retourner une vue genre Accueil
app.get("/", function (req, res) {
  res.render("home");
});

// Création d'une route : GET ALL qui affichera toutes les pollutions
app.get("/datas", (req, res) => {
  db.collection("pollution")
    .find({})
    .toArray(function (err, retour) {
      res.render("datas", { datas: retour });
    });
});

// Création d'une route : GET BY ID
// app.get('/pollutions/:id', (req,res) => {
//   try {
//     const id = parseInt(req.params.id)
//     const pollution = pollutions.find(pollution => pollution.id === id)
//     res.status(200).json(pollution)
//   } catch (error) {
//     console.log(error.body)
//     res.status(500)

//   }
// })

// Création d'une route : Enregistrer une pollution en POST
app.post("/capteur", (req, res) => {
  db.collection("pollution").insertOne(req.body, function (err, result) {
    if (err) res.status(500).json(err);
    else res.status(200).json(req.body);
  });
});

// DELETE route
app.get("/del/:id", (req, res) => {
  const id = parseInt(req.params.id);
  console.log("ID : ", id);
  db.collection("pollution").deleteOne(
    { _id: new ObjectID(req.params.id) },
    function (err, results) {
      if (err) {
        console.log(Object.keys(err));
        // logTruc(err, "\n");
      } else {
        console.log(results);
        logTruc("resultat delete " + results.acknowledged, "\n");
      }
    }
  );
  db.collection("pollution")
    .find({})
    .toArray(function (err, retour) {
      res.render("datas", { datas: retour });
    });
});

// Démarrage du serveur (on notera le "app.get('port')" qui permet d'appeler la variable crée plus haut)
app.listen(app.get("port"), () => {
  console.log(`Serveur à l\'écoute sur le port ${process.env.APP_PORT}`);
});

function logTruc(data) {
  let log = dateNow() + " : " + data + "\n";
  fs.appendFile("logs.txt", log, "utf8", function (err) {
    if (err) return console.log(Object.keys(err));
    console.log("Erreur ecriture logs ", err);
  });
}

function dateNow() {
  let date_ob = new Date();

  // current date
  // adjust 0 before single digit date
  let date = ("0" + date_ob.getDate()).slice(-2);

  // current month
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

  // current year
  let year = date_ob.getFullYear();

  // current hours
  let hours = date_ob.getHours();

  // current minutes
  let minutes = date_ob.getMinutes();

  // current seconds
  let seconds = date_ob.getSeconds();

  return (
    year +
    "-" +
    month +
    "-" +
    date +
    " " +
    hours +
    ":" +
    minutes +
    ":" +
    seconds
  );
}
